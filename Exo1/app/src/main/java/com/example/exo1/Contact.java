package com.example.exo1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Contact {

    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "contact.db";

    private static final String Table_Contact = "contact";
    private static final String COL_ID = "ID";
    private static final int NUM_COL_ID = 0;
    private static final String COL_ISBN = "ISBN";
    private static final int NUM_COL_ISBN = 1;
    private static final String COL_TITRE = "Titre";
    private static final int NUM_COL_TITRE = 2;

    private SQLiteDatabase bdd;

    private BDD maBaseSQLite;

    public Contact(Context context){
        //On crée la BDD et sa table
        maBaseSQLite = new BDD(context, NOM_BDD, null, VERSION_BDD);
    }

    public void open(){
        //on ouvre la BDD en écriture
        bdd = maBaseSQLite.getWritableDatabase();
    }

    public void close(){
        //on ferme l'accès à la BDD
        bdd.close();
    }

    public SQLiteDatabase getBDD(){
        return bdd;
    }

    public long insertLivre(Contact contact){
        ContentValues values = new ContentValues();
        values.put(COL_ISBN, contact.getIsbn());
        values.put(COL_TITRE, contact.getTitre());
        return bdd.insert(Table_Contact, null, values);
    }

    private byte[] getTitre() {

    }

    private byte[] getIsbn() {
    }

    public int upDateContact(int id, Contact contact){
        ContentValues values = new ContentValues();
        values.put(COL_ISBN, contact.getIsbn());
        values.put(COL_TITRE, contact.getTitre());
        return bdd.update(Table_Contact, values, COL_ID + " = " +id, null);
    }
}