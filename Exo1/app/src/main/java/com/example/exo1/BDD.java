package com.example.exo1;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BDD extends SQLiteOpenHelper {



    private SQLiteDatabase maBaseDonnees;
    private static final String REQUETE_CREATION_BD = "create table contact (id primary key not null ,num int,nom varchar(255),prenom varchar(255));";
    public BDD(Context context, String nom, SQLiteDatabase.CursorFactory cursorfactory, int version){
        super(context,nom,cursorfactory,version);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(REQUETE_CREATION_BD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE conatct;");
        onCreate(db);
    }
}
