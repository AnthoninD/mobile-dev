package com.example.exo1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class page2 extends AppCompatActivity {
    private  static ArrayList<String> contact = new ArrayList<String>();
    public String FileName="Fichier";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page2);
        Intent intent = getIntent();
        String nom = "";
        String prenom = "";
        String phone="";
        if (intent.hasExtra("nom")){
            nom = intent.getStringExtra("nom");
        }
        if (intent.hasExtra("prenom")){
            prenom = intent.getStringExtra("prenom");
        }
        if (intent.hasExtra("phone")){
            phone = intent.getStringExtra("phone");
        }
        contact.add(nom+" "+prenom+" "+ phone);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,contact);
        ListView listView = (ListView)findViewById(R.id.myListNom);
        listView.setAdapter(adapter);

        Button button = (Button) findViewById(R.id.files);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FileOutputStream fos = openFileOutput(FileName, Context.MODE_APPEND);
                    fos.write(contact.toString().getBytes());
                    fos.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
